<?php
use Intervals\Interval;

class IntervalTest extends \PHPUnit_Framework_TestCase
{
	/**
     * @expectedException InvalidArgumentException
     */
	public function it_throws_an_exception_when_calling_constructor_with_invalid_arguments()
	{
		new Interval(2, 1);
	}

	/**
	 * @test
	 */
	public function canDigest()
	{
		$i = new Interval(1, 4);
		$j = new Interval(2, 3);
		$this->assertTrue($i->canDigest($j));

		$j = new Interval(2, 5);
		$this->assertFalse($i->canDigest($j));
	}

	/**
	 * @test
	 */
	public function isMoreGreedyThan()
	{
		$i = new Interval(1, 2);
		$j = new Interval(3, 4);
		$this->assertFalse($i->isMoreGreedyThan($j));

		$i = new Interval(1, 4);
		$j = new Interval(3, 5);
		$this->assertTrue($i->isMoreGreedyThan($j));
	}
}