<?php
use Intervals\Intervals;

class IntervalsTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @test
	 */
    public function boundaries()
    {
    	$intervals = new Intervals;
        $intervals->inserts($this->shuffle([
        	[1,4], [10,32], [11,27], [300,301], [307,337]
        ]));
        $this->assertEquals([1, 4, 10, 32, 11, 27, 300, 301, 307, 337], $intervals->boundaries());

        $intervals->clean()->inserts([
            [1,4], [10,32], [10,27], [300,301], [307,337]
        ]);
        $this->assertEquals([1, 4, 10, 32, 10, 27, 300, 301, 307, 337], $intervals->boundaries());
    }

	/**
	 * @test
	 */
    public function containsNFirstIntegers()
    {
        $intervals = new Intervals;

        $intervals->inserts($this->shuffle([
            [9,10], [7,8], [5,6], [3,4], [1,2]
        ]));
        $this->assertTrue($intervals->containsNFirstIntegers());

        $intervals->clean()->inserts($this->shuffle([
            [9,10], [7,8], [5,6], [3,4], [1,1]
        ]));
        $this->assertFalse($intervals->containsNFirstIntegers());
    }

    /**
     * @test
     */
    public function overlaps()
    {
        $intervals = new Intervals;

        $intervals->inserts([
            [1,2]
        ]);
        $this->assertFalse($intervals->overlaps());

        $intervals->clean()->inserts($this->shuffle([
            [1,2], [3,4]
        ]));
        $this->assertFalse($intervals->overlaps());

        $intervals->clean()->inserts($this->shuffle([
            [1,2], [2,4]
        ]));
        $this->assertTrue($intervals->overlaps());

        $intervals->clean()->inserts($this->shuffle([
            [1,3], [2,4]
        ]));
        $this->assertTrue($intervals->overlaps());

        $intervals->clean()->inserts($this->shuffle([
            [1,2], [3,4], [5,6]
        ]));
        $this->assertFalse($intervals->overlaps());

        $intervals->clean()->inserts($this->shuffle([
            [1,2], [3,5], [5,6]
        ]));
        $this->assertTrue($intervals->overlaps());

        $intervals->clean()->inserts($this->shuffle([
            [1,2], [3,6], [5,6]
        ]));
        $this->assertTrue($intervals->overlaps());

        $intervals->clean()->inserts($this->shuffle([
            [1,2], [3,6], [5,7]
        ]));
        $this->assertTrue($intervals->overlaps());

        $intervals->clean()->inserts($this->shuffle([
            [1,2], [3,7], [5,6]
        ]));
        $this->assertFalse($intervals->overlaps());
    }

    /**
     * @test
     */
    public function isValid()
    {
     	$intervals = new Intervals;

        $intervals->inserts($this->shuffle([
        	[9,10], [7,8], [5,6], [3,4], [1,2]
        ]));
        $this->assertTrue($intervals->isValid());

        $intervals->clean()->inserts($this->shuffle([
            [2, 3]
        ]));
        $this->assertFalse($intervals->isValid());

        $intervals->clean()->inserts($this->shuffle([
            [1,2], [4,5]
        ]));
        $this->assertFalse($intervals->isValid());

        $intervals->clean()->inserts($this->shuffle([
            [1,4], [2,3]
        ]));
        $this->assertTrue($intervals->isValid());

        $intervals->clean()->inserts($this->shuffle([
            [1,                                      14],
             [2,            7],  [8,              13],
                [3,4], [5,6],      [9,10], [11,12]
        ]));
        $this->assertTrue($intervals->isValid());

        $intervals->clean()->inserts($this->shuffle([
            [1,                                       14],
             [2,            7],  [8,                13],
                [3,4], [5,         9],[9,10], [11,12]
        ]));
        $this->assertFalse($intervals->isValid());
    }

    protected function shuffle($array)
    {
        shuffle($array);
        return $array;
    }
}
