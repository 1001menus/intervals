<?php
namespace Intervals;

use Closure;
use SplMinHeap;

/**
 * Class Intervals
 * @package Intervals
 */
class Intervals extends SplMinHeap
{
    /**
     * @var array
     */
    protected $backup = [];

    /**
     * @var callable
     */
    protected $parseLeft;

    /**
     * @var callable
     */
    protected $parseRight;

    /**
     *
     */
    public function __construct()
    {
        $this->parseLeft = function ($i) {
            return $i[0];
        };
        $this->parseRight = function ($i) {
            return $i[1];
        };
    }

    /**
     * @param mixed $i
     * @param callable $left
     * @param callable $right
     * @return $this
     */
    public function insert($i, Closure $left = null, Closure $right = null)
    {
        if (!($i instanceof Interval)) {
            $l = $left ?: $this->parseLeft;
            $r = $right ?: $this->parseRight;

            $i = new Interval($l($i), $r($i));
        }

        $this->backup[] = $i;

        parent::insert($i);

        return $this;
    }

    /**
     * @param array $array
     * @return $this
     */
    public function inserts(array $array)
    {
        foreach ($array as $value) {
            $this->insert($value);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function overlaps()
    {
        $overlaps = false;

        $g = $this->extract();
        foreach ($this as $i) {
            if (!$g->isMoreGreedyThan($i)) {
                $g = $i;
                continue;
            }

            if (!$g->canDigest($i)) {
                $overlaps = true;
                break;
            }
        }

        $this->rebuild();

        return $overlaps;
    }

    /**
     * @return bool
     */
    public function containsNFirstIntegers()
    {
        $boundaries = $this->boundaries();
        $n = max($boundaries);

        return array_sum($boundaries) == $n * ($n + 1) / 2;
    }

    /**
     * @return array
     */
    public function boundaries()
    {
        $boundaries = [];
        foreach ($this as $i) {
            $boundaries[] = $i->getLeft();
            $boundaries[] = $i->getRight();
        }
        $this->rebuild();

        return $boundaries;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return !$this->overlaps() && $this->containsNFirstIntegers();
    }

    /**
     * @return $this
     */
    public function clean()
    {
        foreach ($this as $i) ;
        $this->backup = [];

        return $this;
    }

    /**
     * @param callable $left
     * @param callable $right
     */
    public function setDefaultEndpointsParser(Closure $left, Closure $right)
    {
        $this->parseLeft = $left;
        $this->parseRight = $right;
    }

    /**
     * @return void
     */
    protected function rebuild()
    {
        foreach ($this as $i) ;
        $this->inserts($this->backup);
    }

    /**
     * @param Interval $i
     * @param Interval $j
     */
    protected function compare(Interval $i, Interval $j)
    {
        return parent::compare($i->getLeft(), $j->getLeft());
    }
}
